========
Scrabble
========

**Project Description:**

From given rack of seven letters, determine all the valid words.Obtain the highest scoring word.Score of a word can be calculated by adding scores of individual words as per scrabble rules. 

**Input:**

1. A rack of seven letters accepted from command-line.

2. Scoring rule.

3. Official word list ::

	Rules for word list

	- One word per line.
	- Includes new lines.
	- Length 2-15.
	- Alphabetical order.
	- In upper case.


**Output:**

A list of valid words,sorted by score--descending.

**process:**
 
Assume valid input of a rack.
 
1.Generate possible strings of length 2-7 from the rack.
 
2.Check if they are valid, from the provided official word list.

3.Now find the scores of valid words.

4.Print all the valid words along with their scores in sorted order -- descending


